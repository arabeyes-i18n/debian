# translation of debconf_po.po to Arabic
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
#
# Ossama M. Khayat <okhayat@yahoo.com>, 2005, 2006.
msgid ""
msgstr ""
"Project-Id-Version: debconf_po\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-10-05 19:44+0200\n"
"PO-Revision-Date: 2006-02-21 08:18+0300\n"
"Last-Translator: Ossama M. Khayat <okhayat@yahoo.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.2\n"

#: ../Debconf/AutoSelect.pm:76
#, perl-format
msgid "falling back to frontend: %s"
msgstr "التّراجع إلى الواجهة: %s"

# # CHECK
#: ../Debconf/AutoSelect.pm:84
#, perl-format
msgid "unable to initialize frontend: %s"
msgstr "لم يمكن ابتداء الواجهة: %s"

#: ../Debconf/AutoSelect.pm:90
#, perl-format
msgid "Unable to start a frontend: %s"
msgstr "لم يمكن تشغيل واجهة: %s"

#: ../Debconf/Config.pm:130
msgid "Config database not specified in config file."
msgstr "قاعدة بيانات التهيئة غير مُحدّدة في ملف التهيئة."

#: ../Debconf/Config.pm:134
msgid "Template database not specified in config file."
msgstr "قالب قاعدة البيانات غير مُحدّد في ملف التهيئة."

#: ../Debconf/Config.pm:139
msgid ""
"The Sigils and Smileys options in the config file are no longer used. Please "
"remove them."
msgstr ""
"الخيارات Sigils و Smileys في ملف التهيئة غير مستخدمة بعد الآن. الرجاء "
"إزالتها."

# # CHECK
#: ../Debconf/Config.pm:153
#, perl-format
msgid "Problem setting up the database defined by stanza %s of %s."
msgstr "مشكلة في إعداد قاعدة البيانات المُعرفة في المَقْطع %s من %s."

#: ../Debconf/Config.pm:228
msgid ""
"  -f,  --frontend\t\tSpecify debconf frontend to use.\n"
"  -p,  --priority\t\tSpecify minimum priority question to show.\n"
"       --terse\t\t\tEnable terse mode.\n"
msgstr ""
"  -f,  --frontend\t\tتحديد واجهة debconf المطلوب استخدامها.\n"
"  -p,  --priority\t\tتحديد أقل مستوى أولوية للأسئلة المطلوب إظهارها.\n"
"       --terse\t\t\tتمكين وضع terse.\n"

#: ../Debconf/Config.pm:308
#, perl-format
msgid "Ignoring invalid priority \"%s\""
msgstr "تجاهل الأولويّة الغير صالحة \"%s\""

#: ../Debconf/Config.pm:309
#, perl-format
msgid "Valid priorities are: %s"
msgstr "الأولويات الصالحة هي: %s"

#: ../Debconf/Element/Editor/Boolean.pm:30
#: ../Debconf/Element/Editor/Multiselect.pm:31
#: ../Debconf/Element/Editor/Select.pm:31
msgid "Choices"
msgstr "الخيارات"

#: ../Debconf/Element/Editor/Boolean.pm:30
#: ../Debconf/Element/Editor/Boolean.pm:36
#: ../Debconf/Element/Editor/Boolean.pm:59
#: ../Debconf/Element/Teletype/Boolean.pm:28
msgid "yes"
msgstr "نعم"

#: ../Debconf/Element/Editor/Boolean.pm:30
#: ../Debconf/Element/Editor/Boolean.pm:39
#: ../Debconf/Element/Editor/Boolean.pm:62
#: ../Debconf/Element/Teletype/Boolean.pm:29
msgid "no"
msgstr "لا"

# # CHECK
#: ../Debconf/Element/Editor/Multiselect.pm:32
msgid ""
"(Enter zero or more items separated by a comma followed by a space (', ').)"
msgstr "(لا تُدخل أي عنصر أو عنصر أو أكثر مفصولة بفواصل يتبعها مسافة (', ').)"

#: ../Debconf/Element/Gnome.pm:183
msgid "_Help"
msgstr "_مساعدة"

#: ../Debconf/Element/Gnome.pm:185
msgid "Help"
msgstr "مساعدة"

#: ../Debconf/Element/Noninteractive/Error.pm:39
msgid ""
"Debconf was not configured to display this error message, so it mailed it to "
"you."
msgstr "لم تتم تهيئة debconf لعرض رسالة الخطأ هذه، فقام بإرسالها إليك بالبريد."

#: ../Debconf/Element/Noninteractive/Error.pm:63
msgid "Debconf"
msgstr "Debconf"

#: ../Debconf/Element/Noninteractive/Error.pm:86
#, perl-format
msgid "Debconf, running at %s"
msgstr "Debconf، يعمل على %s"

# # CHECK
#: ../Debconf/Element/Select.pm:95 ../Debconf/Element/Select.pm:110
#, perl-format
msgid ""
"Input value, \"%s\" not found in C choices! This should never happen. "
"Perhaps the templates were incorrectly localized."
msgstr ""
"لم يُعثر على قيمة الإدخال، \"%s\" في خيارات C! يجب أن لا يحدث هذا أبداً. يبدو "
"أن القوالب تُرجمت بشكل غير صحيح."

#: ../Debconf/Element/Teletype/Multiselect.pm:27
msgid "none of the above"
msgstr "ليس أياً مما أعلاه"

#: ../Debconf/Element/Teletype/Multiselect.pm:47
msgid "Enter the items you want to select, separated by spaces."
msgstr "أدخل العناصر التي تريد اختيارها، مفصولة بمسافات."

#: ../Debconf/FrontEnd.pm:131
#, perl-format
msgid "Unable to load Debconf::Element::%s. Failed because: %s"
msgstr "لم يمكن تحميل Debconf::Element::%s. فشل ذلك بسبب: %s"

#: ../Debconf/FrontEnd.pm:312
#, perl-format
msgid "Configuring %s"
msgstr "تهيئة %s"

#: ../Debconf/FrontEnd/Dialog.pm:52
msgid "TERM is not set, so the dialog frontend is not usable."
msgstr "TERM ليست مُعيّنة، لذا لا يمكن استخدام واجهة dialog."

#: ../Debconf/FrontEnd/Dialog.pm:55
msgid "Dialog frontend is incompatible with emacs shell buffers"
msgstr "واجهة dialog ليست متوافقة مع مخازن صدفة emacs"

#: ../Debconf/FrontEnd/Dialog.pm:58
msgid ""
"Dialog frontend will not work on a dumb terminal, an emacs shell buffer, or "
"without a controlling terminal."
msgstr ""
"لن يعمل dialog على طرفيّة وهمية، أو مخزن صدفة emacs، أو بدون طرفيّة تحكّم."

#: ../Debconf/FrontEnd/Dialog.pm:104
msgid ""
"No usable dialog-like program is installed, so the dialog based frontend "
"cannot be used."
msgstr ""
"ليس هناك برنامج مشابه لـdialog مثبّت، لذا لا يمكن استخدام الواجهة المبنيّة على "
"dialog."

#: ../Debconf/FrontEnd/Dialog.pm:111
msgid ""
"Dialog frontend requires a screen at least 13 lines tall and 31 columns wide."
msgstr "تتطلب واجهة dialog شاشة بطول 13 سطراً على الأقل وعرض 31 عموداً."

#: ../Debconf/FrontEnd/Dialog.pm:280
msgid "Package configuration"
msgstr "تهيئة الحزمة"

#: ../Debconf/FrontEnd/Editor.pm:94
msgid ""
"You are using the editor-based debconf frontend to configure your system. "
"See the end of this document for detailed instructions."
msgstr ""
"تستخدم الآن واجهة debconf المبنية على المُحرّر لتهيئة نظامك. راجع نهاية هذا "
"المستند للتعليمات المفصلة."

#: ../Debconf/FrontEnd/Editor.pm:111
msgid ""
"The editor-based debconf frontend presents you with one or more text files "
"to edit. This is one such text file. If you are familiar with standard unix "
"configuration files, this file will look familiar to you -- it contains "
"comments interspersed with configuration items. Edit the file, changing any "
"items as necessary, and then save it and exit. At that point, debconf will "
"read the edited file, and use the values you entered to configure the system."
msgstr ""
"واجهة debconf المبنية على المُحرّر تقدم لك ملفاً نصيّاً أو أكثر لتحريرها. وهذا "
"أحد هذه الملفات. إن كنت تألف ملفات تهيئة يونكس القياسيّة، سيكون هذا الملف "
"مألوفاً لك، حيث أنه يحتوي ملاحظات مُدمجة مع عناصر التهيئة. قم بتحرير الملف، "
"بتغيير أي عناصر كما يلزم، ثم احفظ الملف واخرج من البرنامج. عند هذا، سيقرأ "
"debconf الملف، ويستخدم القيم التي أدخلتها لتهيئة نظامك."

#: ../Debconf/FrontEnd/Gnome.pm:96 ../Debconf/FrontEnd/Kde.pm:74
#: ../Debconf/FrontEnd/Kde.pm:85
#, perl-format
msgid "Debconf on %s"
msgstr "Debconf على %s"

#: ../Debconf/FrontEnd/Readline.pm:47
msgid "This frontend requires a controlling tty."
msgstr "تحتاج هذه الواجهة إلى مبرقة (tty) تحكّم."

#: ../Debconf/FrontEnd/Readline.pm:58
msgid "Term::ReadLine::GNU is incompatable with emacs shell buffers."
msgstr "Term::ReadLine::GNU ليس متوافق مع مخازن صدفة emacs."

#: ../Debconf/FrontEnd/Teletype.pm:96
msgid "More"
msgstr "المزيد"

#: ../Debconf/FrontEnd/Web.pm:65
#, perl-format
msgid "Note: Debconf is running in web mode. Go to http://localhost:%i/"
msgstr "ملاحظة: يعمل Debconf في وضع الوب. اذهب إلى http://localhost:%i/"

#: ../Debconf/FrontEnd/Web.pm:165
msgid "Back"
msgstr "السابق"

#: ../Debconf/FrontEnd/Web.pm:167
msgid "Next"
msgstr "التالي"

#: ../Debconf/Template.pm:90
#, perl-format
msgid ""
"warning: possible database corruption. Will attempt to repair by adding back "
"missing question %s."
msgstr ""
"تحذير: احتمال فساد قاعدة البيانات. ستتم محاولة إصلاحها بإعادة السؤال المفقود "
"%s."

#: ../Debconf/Template.pm:203
#, perl-format
msgid ""
"Template #%s in %s has a duplicate field \"%s\" with new value \"%s\". "
"Probably two templates are not properly separated by a lone newline.\n"
msgstr ""
"القالب #%s في %s يحتوي حقلاً مُزدوج \"%s\" بقيمة جديدة \"%s\". على الأرجح أن "
"القالبين الجديدين غير مفصولين بشكل صحيح بسطر جديد.\n"

# # CHECK
#: ../Debconf/Template.pm:228
#, perl-format
msgid "Unknown template field '%s', in stanza #%s of %s\n"
msgstr "## CHECKحقل قالب مجهول '%s'، في المَقْطع #%s من %s\n"

# # CHECK
#: ../Debconf/Template.pm:254
#, perl-format
msgid "Template parse error near `%s', in stanza #%s of %s\n"
msgstr "خطأ في إعراب القالب قُرْب `%s'، في المَقْطع #%s من %s\n"

#: ../Debconf/Template.pm:260
#, perl-format
msgid "Template #%s in %s does not contain a 'Template:' line\n"
msgstr "القالب #%s في %s لا يحتوي على سطر 'Template:'\n"

#: ../dpkg-preconfigure:113
#, perl-format
msgid "unable to re-open stdin: %s"
msgstr "لم يمكن إعادة فتح stdin: %s"

#: ../dpkg-preconfigure:116
#, perl-format
msgid "must specify some debs to preconfigure"
msgstr "يجب تحديد بعض حزم deb لتهيئتها مسبقاً"

#: ../dpkg-preconfigure:121
msgid "delaying package configuration, since apt-utils is not installed"
msgstr "تأجيل تهيئة الحزم، حيث أن apt-utils غير مثبتة"

#: ../dpkg-preconfigure:154 ../dpkg-preconfigure:166
#, perl-format
msgid "apt-extracttemplates failed: %s"
msgstr "فشل apt-extracttemplates: %s"

#: ../dpkg-preconfigure:158 ../dpkg-preconfigure:170
#, perl-format
msgid "Extracting templates from packages: %d%%"
msgstr "استخراج القوالب من الحزم: %d%%"

#: ../dpkg-preconfigure:180
msgid "Preconfiguring packages ...\n"
msgstr "تهيئة الحزم مسبقاً ...\n"

#: ../dpkg-preconfigure:192
#, perl-format
msgid "template parse error: %s"
msgstr "خطأ في إعراب القالب: %s"

#: ../dpkg-preconfigure:206
#, perl-format
msgid "debconf: can't chmod: %s"
msgstr "debconf: لايمكن تنفيذ chmod: %s"

#: ../dpkg-preconfigure:217
#, perl-format
msgid "%s failed to preconfigure, with exit status %s"
msgstr "فشلت تهيئة %s مسبقاً، مع حالة خروج %s"

#: ../dpkg-reconfigure:100
msgid ""
"Usage: dpkg-reconfigure [options] packages\n"
"  -a,  --all\t\t\tReconfigure all packages.\n"
"  -u,  --unseen-only\t\tShow only not yet seen questions.\n"
"       --default-priority\tUse default priority instead of low.\n"
"       --force\t\t\tForce reconfiguration of broken packages."
msgstr ""
"الاستخدام: dpkg-reconfigure [options] packages\n"
"  -a,  --all\t\t\tإعادة تهيئة جميع الحزم.\n"
"  -u,  --unseen-only\t\tإظهار الأسئلة التي لم تظهر من قبل فقط.\n"
"       --default-priority\tاستخدام الأولوية الافتراضية بدلاً من المنخفضة.\n"
"       --force\t\t\tأإجبار إعادة تهيئة الحزم المعطوبة."

#: ../dpkg-reconfigure:113
#, perl-format
msgid "%s must be run as root"
msgstr "يجب تشغيل %s كمستخدم جذر"

#: ../dpkg-reconfigure:146
msgid "please specify a package to reconfigure"
msgstr "الرجاء تحديد حزمة لتهيئتها مسبقاً"

#: ../dpkg-reconfigure:167
#, perl-format
msgid "%s is not installed"
msgstr "%s غير مُثبّتة"

#: ../dpkg-reconfigure:171
#, perl-format
msgid "%s is broken or not fully installed"
msgstr "%s معطوبة أو غير مُثبّتة بالكامل"

#: ../dpkg-reconfigure:250
#, perl-format
msgid "Cannot read status file: %s"
msgstr "لا يمكن قراءة ملف الحالة: %s"

#: ../debconf-communicate:53
msgid "Usage: debconf-communicate [options] [package]"
msgstr "الاستخدام: debconf-communicate [options] [package]"

#: ../debconf-mergetemplate:14
msgid ""
"debconf-mergetemplate: This utility is deprecated. You should switch to "
"using po-debconf's po2debconf program."
msgstr ""
"debconf-mergetemplate: هذه الأداة ملغاة. عليك بالانتقال إلىبرنامج po2debconf "
"الخاص بـpo-debconf."

#: ../debconf-mergetemplate:66
msgid "Usage: debconf-mergetemplate [options] [templates.ll ...] templates"
msgstr ""
"الاستخدام: debconf-mergetemplate [options] [templates.ll ...] templates"

#: ../debconf-mergetemplate:71
msgid ""
"\n"
"        --outdated\t\tMerge in even outdated translations.\n"
"\t--drop-old-templates\tDrop entire outdated templates."
msgstr ""
"\n"
"        --outdated\t\tالدمج حتى مع الترجمات الغير محدثة.\n"
"\t--drop-old-templates\tالإهمال التام للقوالب الغير محدثة."

#: ../debconf-mergetemplate:119
#, perl-format
msgid "%s is missing"
msgstr "%s مفقودة"

#: ../debconf-mergetemplate:123
#, perl-format
msgid "%s is missing; dropping %s"
msgstr "%s مفقودة، إسقاط %s"

# # CHECK
#: ../debconf-mergetemplate:146
#, perl-format
msgid "%s is fuzzy at byte %s: %s"
msgstr "%s مبهمة عند البايت %s: %s"

# # CHECK
#: ../debconf-mergetemplate:151
#, perl-format
msgid "%s is fuzzy at byte %s: %s; dropping it"
msgstr "%s مبهمة عند البايت %s: %s؛ جاري إسقاطها"

#: ../debconf-mergetemplate:168
#, perl-format
msgid "%s is outdated"
msgstr "%s انتهت صلاحيتها"

#: ../debconf-mergetemplate:173
#, perl-format
msgid "%s is outdated; dropping whole template!"
msgstr "%s انتهت صلاحيتها، إسقاط القالب بالكامل"

#: ../debconf:88
msgid "Usage: debconf [options] command [args]"
msgstr "الاستخدام: debconf [options] command [args]"

#: ../debconf:90
msgid ""
"\n"
"  -o,  --owner=package\t\tSet the package that owns the command."
msgstr ""
"\n"
"  -o,  --owner=package\t\tتحديد الحزمة التي تخص الأمر."

#~ msgid "Save (mail) Note"
#~ msgstr "حفظ (البريد) الملاحظة"

#~ msgid "Debconf was asked to save this note, so it mailed it to you."
#~ msgstr "طُلب من debconf حفظ هذه الملاحظة، فقام بإرساله إليك بالبريد."

#~ msgid "Information"
#~ msgstr "معلومات"

#~ msgid "The note has been mailed."
#~ msgstr "تم إرسال هذه الملاحظة بالبريد."

#~ msgid "Error"
#~ msgstr "خطأ"

#~ msgid "Unable to save note."
#~ msgstr "تعذر حفظ الملاحظة."

#~ msgid ""
#~ "Debconf was not configured to display this note, so it mailed it to you."
#~ msgstr "لم تتم تهيئة debconf لعرض هذه الملاحظة، فقام بإرسالها إليك بالبريد."
